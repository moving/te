/*
 * tlib.js, 20120918, gl
 * support infrastructure for the testing of ECMAScript, and HTML code
 */

/*
 * data
 */

var failed = false;

/*
 * methods
 */

function shouldBe(a, b)
{
    var evalA;
    try {
        evalA = eval(a);
    } catch(e) {
        evalA = e;
    }
    
    if (evalA == b || isNaN(evalA)
                          && typeof evalA == 'number'
                          && isNaN(b)
                          && typeof b == 'number')
        print("PASS: '" + a + "' should be '" + b + "' and is.", "green");
    else
        print("__FAIL__: '" + a + "' should be '" + b + "' but instead is '" + evalA + "'.", "red");
}

function shouldThrow(a)
{
    var evalA;
    try {
        eval(a);
    } catch(e) {
        pass(a + " threw: " + e);
        return;
    }

    fail(a + " did not throw an exception.");
}

/*
 * auxiliary functions
 */

/*
 * bludgeonArguments, what?
 *
function bludgeonArguments() { if (0) arguments; return function g() {} }
h = bludgeonArguments();
gc();
 *
 */

function pass(msg)
{
    print("PASS: " + msg, "green");
}

function fail(msg)
{
    print("FAIL: " + msg, "red");
    failed = true;
}

/*
 * 20120920, gl
 * imported, and adapted from ../dlib.hs
 */

function tprint( m )
{
 var contentdiv = document.getElementById('contentdiv');
 contentdiv.contentDocument.body.innerHTML += m;
}

function notImplemented( id )
{
 print( "FAIL: '" + id + "' is not implemented", "red" );
 return "not implemented";
}

/*
 * emit messages into its place within the DOM of a test web page
 * 20120920, gl, now in colour
 */
function print( m, c )
{
 function color( c )
 {
   if ( c == "red" ) return "#ffcc66";
   if ( c == "green" ) return "#ccff99";
 }
  tprint( "<span style='background-color: " + color( c ) + ";'>" + m + "</span><br/>" );
}


