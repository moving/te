/*
 * SQLStore.js
 */

// var systemDB;

function initDB()
{
 // notImplemented( "initDB" );
pass( "initDB" );

 try 
 {
  if (!window.openDatabase)
   {
    fail('window.openDatabase is not supported');
   } else
   {
     pass( "window.openDatabass appears to be supported" );
     var shortName = 'mydatabase';
     var version = '1.0';
     var displayName = 'My Important Database';
     var maxSize = 65536; // in bytes
     var myDB = openDatabase(shortName, version, displayName, maxSize);
/*
 * debug
 */
    pass( "shortName: " + shortName );
    pass( "myDB: " + myDB );
    pass( "typeof myDB: " + typeof myDB );
    pass( "typeof systemDB: " + typeof systemDB );
/*
 *
 */
   }
 } catch(e)
 {
  fail( "initDB fought " + e );
  fail( "initDB catch( INVALID_STATE_ERR ) e: " + INVALID_STATE_ERR );
  if (e == INVALID_STATE_ERR)
  {
   // Version number mismatch.
   fail("Invalid database version.");
  } else
  {
   fail("Unknown error "+e+".");
  }
 return;
 }
    createTables(myDB);
    systemDB = myDB;

    pass( "typeof systemDB: " + typeof systemDB );
}

/*
 * debug section, 20120916, gl
 * 20120918, gl, re-moved to dlib.js, notImplemented(), dprint( txt )
 */

function docLink(row)
{

}

function deleteUpdateResults(transaction, results)
{

}

function reallyDelete(id)
{

}

function deleteFile(id)
{

}

function chooseDialog()
{
 notImplemented( "chooseDialog" );
}

function linkToCreateNewFile()
{

}

function createNewFileAction()
{

}

function saveFile()
{

}

function createNewFile()
{

}

function loadFileData(transaction, results)
{

}

function loadFile(id)
{

}

function createTables(db)
{
pass( "createTables" );
shouldBe( "db", "unknown" );
return;
/*
 * To wipe out the table enable this block.
 *
 if (0)
 {
  db.transaction(
    function (transaction)
     {
     transaction.executeSql('DROP TABLE files;');
     transaction.executeSql('DROP TABLE filedata;');
     }
   );
  }
 *
 */

 db.transaction(
   function (transaction)
    {
     transaction.executeSql( 'CREATE TABLE IF NOT EXISTS files(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, filedata_id INTEGER NOT NULL, deleted INTEGER NOT NULL DEFAULT 0);',
     [],
     nullDataHandler,
     killTransaction
     );

     transaction.executeSql('CREATE TABLE IF NOT EXISTS filedata(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, datablob BLOB NOT NULL DEFAULT "");',
      [],
      nullDataHandler,
      errorHandler
     );
   }
}

function killTransaction(transaction, error)
{
/*
 *  fatal transaction error
 */
 return true;
}

function errorHandler(transaction, error)
{
/*
 * error.message is a human-readable string.
 * error.code is a numeric error code
 */
  fail('Oops. Error was "'+error.message+'" (Code "'+error.code+'")');
/*
 * Handle errors here
 */
  var we_think_this_error_is_fatal = true;
  if (we_think_this_error_is_fatal) return true;
  return false;
}

function nullDataHandler(transaction, results)
{

}

function saveChangesDialog(event)
{

}

function setupEventListeners()
{
 notImplemented( "setupEventListeners" );
}
