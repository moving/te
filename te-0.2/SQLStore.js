/*
 * SQLStore.js
 */

var systemDB;

function initDB()
{
 notImplemented( "initDB" );
}

/*
 * debug section, 20120916, gl
 */
var debug = true;

function dprint( m )
{
// alert( "not implemented" );
 var contentdiv = document.getElementById('contentdiv');
 contentdiv.contentDocument.body.innerHTML += m;
}

function notImplemented( id )
{
 dprint( id + ": is not implemented" + "<br/>" );
}

/*
 * end debug section
 */

function docLink(row)
{

}

function deleteUpdateResults(transaction, results)
{

}

function reallyDelete(id)
{

}

function deleteFile(id)
{

}

function chooseDialog()
{
 notImplemented( "chooseDialog" );
}

function linkToCreateNewFile()
{

}

function createNewFileAction()
{

}

function saveFile()
{

}

function createNewFile()
{

}

function loadFileData(transaction, results)
{

}

function loadFile(id)
{

}

function createTables(db)
{

}

function killTransaction(transaction, error)
{

}

function errorHandler(transaction, error)
{

}

function nullDataHandler(transaction, results)
{

}

function saveChangesDialog(event)
{

}

function setupEventListeners()
{
 notImplemented( "setupEventListeners" );
}
