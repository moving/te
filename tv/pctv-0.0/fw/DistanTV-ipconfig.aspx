<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <link type="text/css" rel="stylesheet" href="common/login.css" />
<!---  <script type="text/javascript" src="common/common.js"></script> 
-->
  <script type="text/javascript" src="../common/jquery/jquery.min.js"></script>
  <script type="text/javascript" src="../common/jquery/jquery.jclock.js"></script>
  <script type="text/javascript" src="../common/jquery/jquery.ui.custom.min.js"></script>
  <script type="text/javascript" src="../common/jquery/jquery.mousewheel.min.js"></script>
  <script type="text/javascript" src="../common/jquery/jquery.jscrollpane.min.js"></script>    
  
  <script src="js/default.js" type="text/javascript"></script>
  <script src="js/settings.js" type="text/javascript"></script>
  <script src="common/UserAgent.js" type="text/javascript"></script>
  
  
  <script type="text/javascript">

    var url = "ipconfig.aspx/?bUserAgentCalled=1";
    try {

      var browserDetector = new BrowserDetector();
      var errorBrowser = !browserDetector.isUpToDate();
      var browserSring = browserDetector.versionText;

      var url = url +"&useragent=" + browserDetector.versionText;
    }
    catch (e) {
    }
    nav(url);


    //-------------------------------------------------------------------------------------------------
    function nav(f) {
      var theUrl = f;
      if (theUrl != "") {
        location.href = theUrl;
      }
    }

     
     
  </script>
  <title>Loading...</title>
</head>
<body>
  <div id="masterpagecontent">
    <noscript>
      <p class="centerError">You must have JavaScript enabled to use this site!</p>
      <p class="centerError">Diese Seite benötigt JavaScript!</p>
    </noscript>
    <div id="nocookies" style="display: none">
      <p class="centerError">You must have Cookies enabled to use this site!</p>
      <p class="centerError">Diese Seite benötigt Cookies!</p>
    </div>
    <script type="text/javascript">
      if (errorCookies) document.getElementById("nocookies").style.display = "";
    </script>
  </div>
</body>
</html>
