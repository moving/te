Pnnacle 73e Firmware
=================

wget http://www.wi-bw.tfh-wildau.de/~pboettch/home/linux-dvb-firmware/dvb-usb-dib0700-1.10.fw 

sudo mv dvb-usb-dib0700-1.10.fw /lib/firmware/


... more about firmware
==================

http://www.linuxtv.org/wiki/index.php/Xceive_XC3028/XC2028#How_to_Obtain_the_Firmware


Linux TV
=======

hg clone http://linuxtv.org/hg/v4l-dvb 

cd v4l-dvb 

sudo make 

sudo make install


dmesg | grep usb
=============

[ 199.336000] usb 5-1: new high speed USB device using ehci_hcd and address 3 
[ 199.468000] usb 5-1: configuration #1 chosen from 1 choice 
[ 199.468000] dvb-usb: found a 'Pinnacle PCTV 73e' in cold state, will try to load a firmware 
[ 199.580000] dvb-usb: downloading firmware from file 'dvb-usb-dib0700-1.10.fw' 
[ 200.268000] dvb-usb: found a 'Pinnacle PCTV 73e' in warm state. 
[ 200.268000] dvb-usb: will pass the complete MPEG2 transport stream to the software demuxer. 
[ 200.316000] dvb-usb: no frontend was attached by 'Pinnacle PCTV 73e' 
[ 200.316000] dvb-usb: schedule remote query interval to 150 msecs. 
[ 200.316000] dvb-usb: Pinnacle PCTV 73e successfully initialized and connected.


Kaffeine
======

sudo apt-get install kaffeine
