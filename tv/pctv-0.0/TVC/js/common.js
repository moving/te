
//-------------------------------------------------------------------------------------------------

function WizardFlags() { };
WizardFlags.AllFlags = 0x000000FF;   // reserve 8 bit for wizard in flags
WizardFlags.Started = 0x01;
WizardFlags.NetworkToWireless = 0x02;
WizardFlags.NetworkToWired = 0x04;
WizardFlags.Scan = 0x10;
WizardFlags.Finished = 0x80;

//=== Cookie ==============================================================================
function Cookie() { };
Cookie.setValue = function(name, value, lifeTimeDays) {
  var cookie = name + "=" + escape(value);
  if (typeof lifeTimeDays != 'undefined') {
    var exp = new Date();
    exp.setTime(exp.getTime() + lifeTimeDays * 24 * 60 * 60 * 1000);
    cookie += "; expires =" + exp.toGMTString();
  }
  cookie += "; path=/TVC";
  document.cookie = cookie;
}

Cookie.getValue = function(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length, c.length));
  }
  return null;
}

Cookie.del = function(name) {
  var exp = new Date();
  exp.setTime(exp.getTime() - 1);
  var cval = Cookie.getValue(name);
  if (cval != null) document.cookie = name + "=" + cval + "; expires =" + exp.toGMTString();
}

//-------------------------------------------------------------------------------------------------
function Detect() { };
Detect.useCompactUI = function() {
  try {
    document.title = navigator.platform;  // @@@hack to see platform value
    if (Detect.isIPhone() || Detect.isIPod())
      return true;
    var min = (screen.width > screen.height) ? screen.height : screen.width;
    return (min < 500);
  } catch (e) { }
  return false;
}

Detect.isIPad = function() {
  return (navigator.platform.search(/ipad/i) >= 0);
}
Detect.isIPhone = function() {
  return (navigator.platform.search(/iphone/i) >= 0);
}
Detect.isIPod = function() {
  return (navigator.platform.search(/ipod/i) >= 0);
}

Detect.isAppleMobile = function() {
  return Detect.isIPad() || Detect.isIPhone() || Detect.isIPod();
}

Detect.isApple = function() {
  return navigator.platform == "MacIntel" || Detect.isAppleMobile();
}

//=== Settings ==============================================================================
function Settings() { };

Settings.LifeTime = 3000;

Settings.getValue = function(key) {
  if (window.localStorage) {
    try {
      var val = window.localStorage.getItem(key);
      return (val == 'undefined' ? null : val);
    } catch (e) {
      return null;
    }
  } else {
    return Cookie.getValue(key);
  }
}

Settings.setValue = function(key, value) {
  if (window.localStorage) {
    try {
      window.localStorage.removeItem(key);
      window.localStorage.setItem(key, value);
    } catch (e) {
      DERR("setValue failed:" + key + " value:" + value + "ec:" + e);
    }
  } else {
    Cookie.setValue(key, value, Settings.LifeTime);
  }
}

Settings.clearValue = function(key) {
  if (window.localStorage)
    window.localStorage.removeItem(key);
  else
    Cookie.del(key);
}

Settings.getLanguage = function() {
  var lang = Settings.getValue('lang');
  if (lang == null) {
    var query = new PageQuery(location.search);
    lang = query.getValue("lang");
    if (lang === false)
      lang = CTranslator.DefaultLanguage("en");
    if (lang != null) 
      Settings.setLanguage(lang); // save as default
  }
  return lang;
}

Settings.setLanguage = function(id) {
  Settings.setValue('lang', id, Settings.LifeTime);
}

//-------------------------------------------------------------------------------------------------
function PageQuery(q) {
  if (q.length > 1)
    this.q = q.substring(1, q.length);
  else
    this.q = null;
  this.keyValuePairs = new Array();
  if (q) {
    for (var i = 0; i < this.q.split("&").length; i++) {
      this.keyValuePairs[i] = this.q.split("&")[i];
    }
  }

  this.getKeyValuePairs = function() { return this.keyValuePairs; }

  this.getValue = function(s) {
    for (var j = 0; j < this.keyValuePairs.length; j++) {
      if (this.keyValuePairs[j].split("=")[0] == s)
        return unescape(this.keyValuePairs[j].split("=")[1]);
    }
    return false;
  }

  this.getParameters = function() {
    var a = new Array(this.getLength());
    for (var j = 0; j < this.keyValuePairs.length; j++) {
      a[j] = this.keyValuePairs[j].split("=")[0];
    }
    return a;
  }

  this.getLength = function() { return this.keyValuePairs.length; }
}

//-------------------------------------------------------------------------------------------------
function loadJsFile(filename) {
  var fileref = document.createElement('script')
  fileref.setAttribute("type", "text/javascript")
  fileref.setAttribute("src", filename)
  document.getElementsByTagName("head")[0].appendChild(fileref)
}

//-------------------------------------------------------------------------------------------------
function loadCssFile(filename) {
  var fileref = document.createElement("link")
  fileref.setAttribute("rel", "stylesheet")
  fileref.setAttribute("type", "text/css")
  fileref.setAttribute("href", filename)
  document.getElementsByTagName("head")[0].appendChild(fileref)
}

//-------------------------------------------------------------------------------------------------
function GetValueFromParams(parameters, seperator, name) {
  var params = parameters.split(seperator);
  var lowerName = name.toLowerCase();
  for (var i = 0; i < params.length; ++i) {
    var param = params[i].split("=");
    if (param[0].toLowerCase() == lowerName) return unescape(param[1]);
  }
}

//-------------------------------------------------------------------------------------------------
function GetUrlParam(name) {
  if (window.location.search != "") {
    return GetValueFromParams(window.location.search.substr(1), "&", name);
  }
}

//-------------------------------------------------------------------------------------------------
function DERR(message) {
  // alert(message);
}

//-------------------------------------------------------------------------------------------------
function D1(message) {
  console.debug(message);
}

//-------------------------------------------------------------------------------------------------
// extends Date with powerful format function
var dateFormat = function() {
  var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function(val, len) {
		  val = String(val);
		  len = len || 2;
		  while (val.length < len) val = "0" + val;
		  return val;
		};

  // Regexes and supporting functions are cached through closure
  return function(date, mask, utc) {
    var dF = dateFormat;

    // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
    if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
      mask = date;
      date = undefined;
    }

    // Passing date through Date applies Date.parse, if necessary
    date = date ? new Date(date) : new Date;
    if (isNaN(date)) throw SyntaxError("invalid date");

    mask = String(dF.masks[mask] || mask || dF.masks["default"]);

    // Allow setting the utc argument via the mask
    if (mask.slice(0, 4) == "UTC:") {
      mask = mask.slice(4);
      utc = true;
    }

    var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			  d: d,
			  dd: pad(d),
			  ddd: dF.i18n.dayNamesShort[D],
			  dddd: dF.i18n.dayNames[D],
			  m: m + 1,
			  mm: pad(m + 1),
			  mmm: dF.i18n.monthNamesShort[m],
			  mmmm: dF.i18n.monthNames[m],
			  yy: String(y).slice(2),
			  yyyy: y,
			  h: H % 12 || 12,
			  hh: pad(H % 12 || 12),
			  H: H,
			  HH: pad(H),
			  M: M,
			  MM: pad(M),
			  s: s,
			  ss: pad(s),
			  l: pad(L, 3),
			  L: pad(L > 99 ? Math.round(L / 10) : L),
			  t: H < 12 ? "a" : "p",
			  tt: H < 12 ? "am" : "pm",
			  T: H < 12 ? "A" : "P",
			  TT: H < 12 ? "AM" : "PM",
			  Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			  o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			  S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

    return mask.replace(token, function($0) {
      return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
    });
  };
} ();


// Some common format strings
dateFormat.masks = {
  "default": "ddd mmm dd yyyy HH:MM:ss",
  shortDate: "m/d/yy",
  mediumDate: "mmm d, yyyy",
  longDate: "mmmm d, yyyy",
  fullDate: "dddd, mmmm d, yyyy",
  shortTime: "h:MM TT",
  mediumTime: "h:MM:ss TT",
  longTime: "h:MM:ss TT Z",
  isoDate: "yyyy-mm-dd",
  isoTime: "HH:MM:ss",
  isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
  isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
  dayNamesShort: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
	],
  dayNames: [
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
  monthNamesShort: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
	],
  monthNames: [
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};


//=== Translator stuff ============================================================================

function CTranslator(lang) {
  // properties...
  this.Table = _Load(lang);
  this.Language = lang;
  // methods...
  this.Localize = function(id) { return this.Table["T_" + id]; }
  this.LocalizeStatic = _LocalizeStatic;
  return;

  //-------------------------------------------------------------------------------------------------
  function _Load(lang) {
    var table = new Array();
    $.ajax({ type: "GET",
      url: "/TVC/lang/TVC_en.xml",
      dataType: "xml",
      async: false,
      success: function(xml) { _Setup(xml, table); }
    });
    if (lang != "en") {
      $.ajax({ type: "GET",
        url: "/TVC/lang/TVC_" + lang + ".xml",
        dataType: "xml",
        async: false,
        success: function(xml) { _Setup(xml, table); }
      });
    }
    return table;
  }

  //-------------------------------------------------------------------------------------------------
  function _Setup(xhr, table) {
    var transTab = table;
    var newTitle = $(xhr).find('string').each(function() {
      var id = $(this).attr('id');
      id = "T_" + id;
      transTab[id] = $(this).text();
    });
  }

  //-------------------------------------------------------------------------------------------------
  function _LocalizeStatic(lang) {
    if (lang !== undefined && lang !== this.Language) {
      // language switched => reload translation table
      this.Table = _Load(lang);
      this.Language = lang;
    }

    var tab = this.Table;
    // handle document title => use extra handling because of IE8
    document.title = tab["T_" + $("Title")[0].id];

    // handle all other static elements start with "T_"  
    var subset = $("[id^='T_']");
    subset.each(function() {
      var id = $(this).attr("id");
      ($(this)[0]).innerHTML = tab[id];
    });

    // localize day/month names
    dateFormat.i18n.shortYearCutoff = 2000; //  number - the cutoff year for determining the century (optional)
    dateFormat.i18n.dayNamesShort = this.Localize("DayNamesShort").split(','); // string[7] - abbreviated names of the days from Sunday (optional)
    dateFormat.i18n.dayNames = this.Localize("DayNames").split(','); // string[7] - names of the days from Sunday (optional)
    dateFormat.i18n.monthNamesShort = this.Localize("MonthNamesShort").split(','); // string[12] - abbreviated names of the months (optional)
    dateFormat.i18n.monthNames = this.Localize("MonthNames").split(','); // string[12] - names of the months (optional)

    // toDo: setup dateFormat.masks with regional settings
  }

  //-------------------------------------------------------------------------------------------------
} // end  CTranslator(lang)

//-------------------------------------------------------------------------------------------------
CTranslator.DefaultLanguage = function (defaultOnFailure) {
  var lang = window.navigator.userLanguage || window.navigator.browserLanguage || window.navigator.language;
  if (lang != null && lang.length != 2) lang = lang.substr(0, 2);
  if (lang == null) lang = defaultOnFailure;
  return lang;
}

//=================================================================================================
BrowserDetector = function() {
  this.isUpToDate = _isUpToDate;
  this.browser = _getBrowser;

  var _browser = null;
  return;

  //---------------------------------------------------------------------------------------------
  function _isUpToDate() {
    var browser = _getBrowser();
    switch (browser.name) {
      case "Chrome":
        if (browser.version < 7) return false;
        break;
      case "Firefox":
        if (browser.version < 3 || (browser.version == 3 && browser.subversion < 5)) return false;
        break;
      case "MSIE":
        if (browser.version < 8) return false;
        break;
      case "Opera":
        if (browser.version < 9 || (browser.version == 9 && browser.subversion < 8)) return false;
        break;
      case "Safari":
        if (browser.version < 4) return false;
        break;
    }
    return true;
  }

  //---------------------------------------------------------------------------------------------
  function _getBrowser() {
    if (_browser == null) {
      _browser = detectBrowser();
      if (_browser == undefined) _browser = Object({ 'name': "unknown", 'version': 1, 'subversion': 0 });
    }
    return _browser;
  }

  //---------------------------------------------------------------------------------------------
  function detectBrowser() {
    var browser = detectFirefox();
    if (browser != null) return browser;
    browser = detectOpera();  // Note: first detect Opera, then MSIE
    if (browser != null) return browser;
    browser = detectMSIE();
    if (browser != null) return browser;
    browser = detectChrome(); // Note: first detect Chrome, then Safari
    if (browser != null) return browser;
    browser = detectSafari();
    if (browser != null) return browser;
  }

  //---------------------------------------------------------------------------------------------
  function detectChrome() {
    var temp = navigator.userAgent.indexOf("Chrome/");
    if (temp < 0) return null;

    var version = navigator.userAgent.substring(temp + ("Chrome/").length);
    version = version.split(' ');
    version = version[0].split('.');
    return new Object({ 'name': "Chrome", 'version': parseInt(version[0]), 'subversion': parseInt(version[1]) });
  }

  //---------------------------------------------------------------------------------------------
  function detectFirefox() {
    var temp = navigator.userAgent.indexOf("Firefox");
    if (temp < 0) return null;

    var version = navigator.userAgent.substring(temp + ("Firefox").length + 1);
    version = version.split('.');
    return new Object({ 'name': "Firefox", 'version': parseInt(version[0]), 'subversion': parseInt(version[1]) });
  }

  //---------------------------------------------------------------------------------------------
  function detectMSIE() {
    var temp = navigator.userAgent.indexOf("; MSIE ");
    if (temp < 0) return null;

    var version = navigator.userAgent.substring(temp + ("; MSIE ").length);
    version = version.substring(0, version.indexOf(";"));
    version = version.split('.');
    return new Object({ 'name': "MSIE", 'version': parseInt(version[0]), 'subversion': parseInt(version[1]) });
  }

  //---------------------------------------------------------------------------------------------
  function detectOpera() {
    var temp = navigator.userAgent.indexOf("Opera");
    if (temp < 0) return null;

    var version = navigator.userAgent.substring(temp + ("Opera").length + 1);
    version = version.split(' ');
    version = version[0].split('.');
    return new Object({ 'name': "Opera", 'version': parseInt(version[0]), 'subversion': parseInt(version[1]) });
  }

  //---------------------------------------------------------------------------------------------
  function detectSafari() {
    var temp = navigator.userAgent.indexOf("Safari");
    if (temp < 0) return null;

    temp = navigator.userAgent.indexOf("Version");
    if (temp < 0) return new Object({ 'name': "Safari", 'version': 2, 'subversion': 0 }); // < 3

    var version = navigator.userAgent.substring(temp + ("Version").length + 1);
    version = version.split(' ');
    version = version[0].split('.');
    return new Object({ 'name': "Safari", 'version': parseInt(version[0]), 'subversion': parseInt(version[1].substring(0, 1)) });
  }
}

//=================================================================================================
