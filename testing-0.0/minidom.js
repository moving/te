/*
 * File: minidom.js
 */

/*
 * print
 */
function print( m )
{
 document.write( "<br/>Print: " + m );
}

function shouldBe(a, b)
{
    var evalA;
    try {
        evalA = eval(a);
    } catch(e) {
        evalA = e;
    }
    
    if (evalA == b || isNaN(evalA) && typeof evalA == 'number' && isNaN(b) && typeof b == 'number')
        print("PASS: " + a + " should be " + b + " and is.", "green");
    else
        print("__FAIL__: " + a + " should be " + b + " but instead is " + evalA + ".", "red");
}

function test()
{
    print("Node is " + Node);
    for (var p in Node)
        print(p + ": " + Node[p]);
    
    node = new Node();
    print("node is " + node);
    for (var p in node)
        print(p + ": " + node[p]);

    child1 = new Node();
    child2 = new Node();
    child3 = new Node();
    
    node.appendChild(child1);
    node.appendChild(child2);

    var childNodes = node.childNodes;
    
    for (var i = 0; i < childNodes.length + 1; i++) {
        print("item " + i + ": " + childNodes.item(i));
    }
    
    for (var i = 0; i < childNodes.length + 1; i++) {
        print(i + ": " + childNodes[i]);
    }

    node.removeChild(child1);
    node.replaceChild(child3, child2);
    
    for (var i = 0; i < childNodes.length + 1; i++) {
        print("item " + i + ": " + childNodes.item(i));
    }

    for (var i = 0; i < childNodes.length + 1; i++) {
        print(i + ": " + childNodes[i]);
    }

    try {
        node.appendChild(null);
    } catch(e) {
        print("caught: " + e);
    }
    
    try {
        var o = new Object();
        o.appendChild = node.appendChild;
        o.appendChild(node);
    } catch(e) {
        print("caught: " + e);
    }
    
    try {
        node.appendChild();
    } catch(e) {
        print("caught: " + e);
    }
    
    oldNodeType = node.nodeType;
    node.nodeType = 1;
    shouldBe("node.nodeType", oldNodeType);
    
    shouldBe("node instanceof Node", true);
    shouldBe("new Object() instanceof Node", false);
    
    print(Node);
}

test();
