/*
 * SQLStore.js
 */

var systemDB;

function initDB()
{
 notImplemented( "initDB" );
}

/*
 * debug section, 20120916, gl
 */
var debug = true;
var dn = 0;
var contentdiv = document.getElementById('contentdiv');
var c = contentdiv.contentDocument.body.innerHTML;

function dprint( a, b, c, d )
{
// alert( "not implemented" );
 function table( )
 {
 if ( c > 0 ) return;
 c += "\n";
 c += "<table>";
 alert( "dprint.table( "+c+" )" );
 }

 function tr( )
 {
 c += "\n";
 c += "<tr>";
 alert( "dprint.tr( "+c+" )" );
 }

 function td( txt )
 {
 c += "<td>";
 if (!txt) txt = "&nbsp";
 c += txt;
 c += "</td>";
 c += "\n";
 alert( "dprint.td( "+c+" )" );
 }

// c += m;
 alert( "dprint"+c );
 table();
 tr();
 td( a );
 td( b );
 td( c );
 td( d );
}

function notImplemented( id )
{
 dprint( id, "is not implemented" );
}

function beingImplemented( id )
{
 dprint( id, "is not implemented yet", "Implementation being in progress" );
}

/*
 * end debug section
 */

function docLink(row)
{

}

function deleteUpdateResults(transaction, results)
{

}

function reallyDelete(id)
{

}

function deleteFile(id)
{

}

function chooseDialog()
{
 notImplemented( "chooseDialog" );
}

function linkToCreateNewFile()
{

}

function createNewFileAction()
{

}

function saveFile()
{

}

function createNewFile()
{

}

function loadFileData(transaction, results)
{

}

function loadFile(id)
{

}

function createTables(db)
{

}

function killTransaction(transaction, error)
{

}

function errorHandler(transaction, error)
{

}

function nullDataHandler(transaction, results)
{

}

function saveChangesDialog(event)
{

}

function setupEventListeners()
{
 notImplemented( "setupEventListeners" );
}
