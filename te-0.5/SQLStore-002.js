/*
 * SQLStore.js
 */

var systemDB;

function initDB()
{
 notImplemented( "initDB" );
}

/*
 * debug section, 20120916, gl
 */
var debug = true;
var dn = 0;

function dprint( a, b, c, d )
{

 function table( )
 {
 return "\n<pre>" + "<table border=1>";
 }

 function tr( )
 {
 return "\n" + "<tr>";
 }

function tr_( )
 {
 return "</tr>";
 }

 function td( txt )
 {
 if (!txt) txt = "&nbsp";
 return "<td>" + txt + "</td>";
 }

 var t = "";
 if ( dn == 0) t += table(); 
 dn += 1;
 t += tr();
 t += td( dn );
 t += td( a );
 t += td( b );
 t += td( c );
 t += td( d );
 t += tr_();
 alert( t );
 var contentdiv = document.getElementById('contentdiv');
 contentdiv.contentDocument.body.innerHTML += t;
}

function dprintClose()
{
 alert( "dprintClose()" );
 var contentdiv = document.getElementById('contentdiv');
 contentdiv.contentDocument.body.innerHTML += " dprintClose() </table>";
}

function notImplemented( id )
{
 dprint( id, "is not implemented" );
}

function beingImplemented( id )
{
 dprint( id, "is not implemented yet", "Implementation being in progress" );
}

/*
 * end debug section
 */

function docLink(row)
{

}

function deleteUpdateResults(transaction, results)
{

}

function reallyDelete(id)
{

}

function deleteFile(id)
{

}

function chooseDialog()
{
 notImplemented( "chooseDialog" );
}

function linkToCreateNewFile()
{

}

function createNewFileAction()
{

}

function saveFile()
{

}

function createNewFile()
{

}

function loadFileData(transaction, results)
{

}

function loadFile(id)
{

}

function createTables(db)
{

}

function killTransaction(transaction, error)
{

}

function errorHandler(transaction, error)
{

}

function nullDataHandler(transaction, results)
{

}

function saveChangesDialog(event)
{

}

function setupEventListeners()
{
 notImplemented( "setupEventListeners" );
}
